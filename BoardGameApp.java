import java.util.Scanner;
public class BoardGameApp{
	public static void main(String[] args){
		Board board = new Board();
		boolean gameOver = false;
		int turns = 8;
		int numCastles = 7;
		
		while(turns > 0 && numCastles > 0){
			
			System.out.println(board);
			System.out.println("You have " + turns + " turns left.");
			System.out.println("And you have " + numCastles + " castles left to place.");
			
			boolean emptyTile = false;
			while(!emptyTile){
				int row = getRow(board);
				int col = getColumn(board);
				int tile = board.placeToken(row,col);
				if( tile == 0){
					System.out.println("That tile was empty, good guess!");
					turns -= 1;
					numCastles -= 1;
					emptyTile = true;
				}
				else if (tile == 1){
					System.out.println("Sorry, that placement occupied by a hidden wall");
					turns -= 1;
					emptyTile = true;
				}
				else{
					System.out.println(board);
					System.out.println("Sorry, that placement is invalid");
				}
			}
		}
		System.out.println(board);
		if(numCastles == 0){
			System.out.println("Congratulations, you won!");
		}
		else{
			System.out.println("Unfortunately, you lost.");
		}
		
		
	}
	//Asks the user to input a row number
	public static int getRow(Board board){
		Scanner reader = new Scanner(System.in);
		while(true){
			System.out.println("Please input a row number");
			int row = reader.nextInt();
			if(row >= 0 && row < board.getSize()){
				return row;
			}
			else{
				System.out.println("Invalid; please enter a number from 1-3");
			}
		}
	}
	
	//Asks the user to input a column number
	public static int getColumn(Board board){
		Scanner reader = new Scanner(System.in);
		
		while(true){
			System.out.println("Please input a column number");
			int col = reader.nextInt();
			if(col >= 0 && col < board.getSize()){
				return col;
			}
			else{
				System.out.println("Invalid; please enter a number from 1-3");
			}
		}
	}	
}