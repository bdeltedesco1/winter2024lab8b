import java.util.Random;
public class Board{
	private final int SIZE = 5;
	private Tile[][] grid;
	
	//Creates a 3x3 Tile array and assings each element as "_"
	public Board(){
		Random rand = new Random();
		this.grid = new Tile[SIZE][SIZE];
		for(int i = 0; i < this.grid.length; i++){
			for(int j = 0; j < this.grid[i].length; j++){
				this.grid[i][j] = Tile.BLANK;
			}
		}
		
		for(int i = 0; i< this.grid.length; i++){
			this.grid[i][rand.nextInt(this.grid.length)] = Tile.HIDDEN_WALL;
		}
		/*
		//Delete when done testing
		this.grid[0][0] = Tile.HIDDEN_WALL;
		this.grid[0][1] = Tile.CASTLE;*/
	}
	
	//Prints the board with numbers on the left and bottom to lable the rows and columns
	public String toString(){
		String printedGrid = "";
		for(int i = 0; i < this.grid.length; i++){
			for(int j = 0; j < this.grid[i].length; j++){
				printedGrid += this.grid[i][j];
			}
			printedGrid += "\n";
		}
		return printedGrid;
	}
	
	//Checks if the tile is blank, if it is, the method places the according player's token
	public int placeToken(int row, int col){
		if(row < SIZE && col < SIZE){
			if (this.grid[row][col].equals(Tile.WALL) || this.grid[row][col].equals(Tile.CASTLE)){
				return -1;
			}
			else if(this.grid[row][col].equals(Tile.BLANK)){
				this.grid[row][col] = Tile.CASTLE;
				return 0;
			}
			else{
				this.grid[row][col] = Tile.WALL;
				return 1;
			}
		}
		else{
			return -2;
		}
	}
	/*
	//Ckecks if the board is full
	public boolean checkFull(){
		boolean full = true;
		for(int i = 0; i < this.grid.length; i++){
			for(int j = 0; j < this.grid[i].length; j++){
				if(this.grid[i][j] == Tile.BLANK){
					full= false;
				}
			}
		}
		return full;
	}
	
	//Checks if a player won horizontally
	private boolean checkIfWinningHorizontal(Tile playerToken){
		for(int i = 0; i < this.grid.length; i++){
			int counter = 0;
			for(int j = 0; j < this.grid[i].length; j++){
				if(this.grid[i][j] == playerToken){
					counter += 1;
				}
				else{
					break;
				}
			}
			if (counter == SIZE){
				return true;
			}
		}
		return false;
	}
	
	//Checks if a player won vertically
	private boolean checkIfWinningVertical(Tile playerToken){
		for(int i = 0; i < this.grid.length; i++){
			int counter = 0;
			for(int j = 0; j < this.grid[i].length; j++){
				if(this.grid[j][i] == playerToken){
					counter += 1;
				}
			}
			if (counter == SIZE){
				return true;
			}
		}
		return false;
	}
	
	//Checks if a player won diagonally, going from top left to bottom right
	private boolean checkIfWinningDiagonalTop(Tile playerToken){
		int counter = 0;
		for(int i = 0; i < this.grid.length; i++){
			int j = i;
			if(this.grid[i][j] == playerToken){
				counter ++;
			}
		}
		if (counter == SIZE){
			return true;
		}
		return false;
	}
	
	//Checks if a player won diagonally, going from bottom left to top right
	private boolean checkIfWinningDiagonalBottom(Tile playerToken){
		int counter = 0;
		int j = 0;
		for(int i = this.grid.length-1; i >= 0; i--){
			if(this.grid[i][j] == playerToken){
				counter ++;
			}
			j++;
		}
		if (counter == SIZE){
			return true;
		}
		return false;
	}
	
	//Checks if a player won
	public boolean checkIfWinning(Tile playerToken){
		if(checkIfWinningHorizontal(playerToken)){
			return true;
		}
		else if(checkIfWinningVertical(playerToken)){
			return true;
		}
		else if(checkIfWinningDiagonalTop(playerToken)){
			return true;
		}
		else if(checkIfWinningDiagonalBottom(playerToken)){
			return true;
		}
		else{
			return false;
		}
	}*/
	
	//Returns the size of the board
	public int getSize(){
		return this.SIZE;
	}
}