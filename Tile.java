public enum Tile{
	BLANK("- "),
	HIDDEN_WALL("H "),
	WALL("W "),
	CASTLE("C ");
	
	private final String name;
	
	private Tile(final String name){
		this.name = name;
	}

	public String getName(){
		return this.name;
	}
	
	public String toString(){
		return this.name;
	}
}